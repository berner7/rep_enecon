## Replication Code for
# Do Energy Efficiency Improvements Reduce Energy Use? Empirical Evidence on the Economy-Wide Rebound Effect in Europe and the United States
## *Anne Berner, Stephan Bruns, Alessio Moneta, David Stern*


This repository replicates the main results of the [paper](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3854876). The repository should help to understand the methodological approach by sharing the underlying code and data for researchers interested in replicating the analysis using the same data or adapting it to new datasets.

The explanations throughout the code are kept short. Please see the paper for more explanations or contact the corresponding author.

---

## Data

The data folder contains one csv with monthly data. These time series are constructed as outlined in the data appendix and they are deseasonalized using the x11 procedure in the package **seasonal**.  

Data before these data processing steps is available from the authors. 


---

## Code

#### data_plot.r
This code produces Figure 2 (Time series data for the countries included in the analysis), 
and Figure G.24 (Energy use data for the European countries including primary energy use).

#### factor_extraction.r 
This code produces the estimates for the latent factors based on a large panel of
time series. It reproduces the results of:
* Table 1 (Explained variance in the set of country-specific time series)
* Figure 3 (Estimated latent factors)
* Figure 4 (Example factor loadings)
* Figure E.16 (The 15 highest factor loadings considering the first two factors)

#### sfavar_two_factors.r
This code produces the estimates for the S-FAVARs with three main variables (energy, energy
prices and GDP) and two factors. Additionally it includes:
  
* Table D.5 (Component-wise normality tests for the different time series)
* Table E.6 (Optimal lag length selection based on the AIC.)
* Figure 5 and B.9 (Forecast error variance decomposition for energy use.)
* Figure C.11-C.15 (IRF plots with bootstraped confidence intervals)
* Figure 6 (Impulse response functions of energy use with respect to an energy efficiency shock (a) and
estimated rebound effects (b))
* Table C.4 (Contemporaneous reaction of the variables to different shocks)
* Figure E.19 (Impulse-response functions resulting from various identification strategies.)

#### comp_elec_fuel.r
Produces Figure G.23 (Estimated rebound effect comparison between the original model and the model including electricity use)

#### rebound_time_dep.r
Produces Figure E.17 (Rebound effect estimations using samples on different time periods) and provides
the code for a historical decomposition and a counterfactual analysis that was not included in the paper.

#### comp_svar_favar.r
Produces Figure E.20 (Comparison of the rebound estimation using the SVAR and the FAVAR model)

#### kalman_filtered_vs_original.r
Produces Figure A.7 and Figure A.8 (Comparison between the interpolated series and the quarterly GDP data for
European countries) 
The data series in the folder Matlab were produced using the code of Mönch and Uhlig (2005) Towards a monthly business cycle chronology for the euro area. *Journal of Business Cycle Measurement and Analysis*, 2005(1):43–69.

#### main_singular_rebound_us_uk.R
Produces Figure F.21 and F 22 (Impulse response functions for the US, identified with the distance covariance approach using a singular SVAR approach with three shocks.)
The SVAR is identified here using the singular SVAR approach (C-ICA) by Cordoni, F. and Corsi, F. (2019). Identification of singular and noisy structural VAR models: The collapsing-ICA approach. Available at SSRN 3415426.

### Folder "Functions"

This folder contains helper functions that are used in the three main R-files. The functions
are described at the beginning of each of the above R-files.

---

## Results
This folder contains *.RDATA files that are produced by the code and then
used as an input for other parts of the code. This folder also contains some of the final results, but most of the final
results are directly shown in R.