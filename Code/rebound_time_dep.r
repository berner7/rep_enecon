#------------------------------------
# This code produces the results for 
#	the S-FAVAR using two factors
#------------------------------------

#-----------------------------------
# Packages
#-----------------------------------
library(dplyr); 
library(tidyr);  
library(zoo); 
library(ggplot2); 
library(plyr); 
library(purrr)
library(vars)
library(svars)
library(xtable)
library(lubridate)
library(normtest)
library(cowplot)

#---------------------------------------
# Paths
#---------------------------------------
loadpath <- "Data/" #change this
savepath <- "Results/" #change this

#---------------------------------------
# Data
#---------------------------------------
# monthly data
favar_dat2 <- readRDS(paste(loadpath, "favar_data_2factors.rds", sep=""))

#---------------------------------------
# Functions
#---------------------------------------
# (1) Gauss tests  
source("Code/Functions/gauss_tests.r")
# (2) Reduced-form estimation 
source("Code/Functions/var_prep.r")
# (3) FEVD decomposition plot 
source("Code/Functions/fevd_plot.r")
# (4) Transform bootstrap data to rebound effect (including confidence bans) and plot effect
source("Code/Functions/boot_eu.r")
# (5) Plot IRF of efficiency shock for comparing identification strategies
source("Code/Functions/plot_identif_comp.r")
# (6) Confidence Intervals for estimated energy efficiency shock and IRF 
source("Code/Functions/confidence_re.r")
# (7) Plot all IRF for comparing identification strategies 
source("Code/Functions/plot_boot_comp.r")
# (8) Transform bootstrap data to rebound effect (including confidence bans) and plot effect
source("Code/Functions/plot_re.r")
# (9) Plot selected historical decomposition combinations
source("Code/Functions/plot_sel_hd.r")
#---------------------------------------
# Estimation of reduced form and Gauss Tests
#---------------------------------------

# Estimate reduced-form FAVARs (compare lag.max = 6 and 12)
favar2008 <- favar_dat2 %>%
  dplyr::select(country, data = fac_est)%>%
  mutate(var6 = map(data, var_prep, log = TRUE))%>%
  mutate(red_form08 = map(var6, "vars"))%>%
  mutate(data = map(data, ~filter(.x, time >= as.Date("2009-01-01"))))%>%
  mutate(var6 = map(data, var_prep, log = TRUE))%>%
  mutate(red_form09 = map(var6, "vars"))%>%
  mutate(data = map(data, ~filter(.x, time >= as.Date("2010-01-01"))))%>%
  mutate(var6 = map(data, var_prep, log = TRUE))%>%
  mutate(red_form10 = map(var6, "vars"))
  

# S-FAVAR estimation with different identification methods (dcov, st, chol, ngml)
favars <- favar2008%>%
  dplyr::select(country, red_form08, red_form09, red_form10)%>%
  mutate(svar08 = map(red_form08, id.dc))%>%
  mutate(svar09 = map(red_form09, id.dc))%>%
  mutate(svar10 = map(red_form10, id.dc))


# B matrices
B <- favars  %>% 
  mutate(B_dc = map(svar08, "B"), B_ngml = map(svar09, "B"), B_chol = map(svar10, "B")) %>% 
  dplyr::select(country, B_dc, B_ngml, B_chol)


# Bootstrap 
set.seed(42)
favars <- favars %>% mutate(
  boot_08 = map(svar08, boot_eu),
  boot_09 = map(svar09, boot_eu),
  boot_10 = map(svar10, boot_eu)
)
saveRDS(favars, paste0(savepath, "favars_time_dep.rds"))
favars <- readRDS(paste0(savepath, "favars_time_dep.rds"))

# Impulse-response function plots
favar_plots <- favars %>% 
  mutate(plots = lapply(map(favars$boot_10,"bootstrap"), plot, lowerq = 0.1, upperq = 0.9))

favar_plots %>% 
  dplyr::select(country, plots)%>%
  pmap(function(country, plots){
    ggsave(filename = paste0(savepath, "fig_c_irf_favars2_10", "_",
                             country, ".pdf"), plots+xlim(0,24), width = 8, height = 6)})

#-------------------------------------------------------------------------
# Rebound effect calculation
#-------------------------------------------------------------------------
### Barplot (Figure 6) 
temp <- favars %>% 
  dplyr::select(country, boot_08, boot_09, boot_10)%>%
  mutate(ci_data =map(map(boot_08, "bootstrap"), confidence_re, lowerq = 0.1, upperq = 0.9))%>%
  mutate(ci_data08 = map(ci_data, function(x) apply(x, 2, function(x) 1 - (x/x[1]))))%>%
  mutate(ci_data =map(map(boot_09, "bootstrap"), confidence_re, lowerq = 0.1, upperq = 0.9))%>%
  mutate(ci_data09 = map(ci_data, function(x) apply(x, 2, function(x) 1 - (x/x[1]))))%>%
  mutate(ci_data =map(map(boot_10, "bootstrap"), confidence_re, lowerq = 0.1, upperq = 0.9))%>%
  mutate(ci_data10 = map(ci_data, function(x) apply(x, 2, function(x) 1 - (x/x[1]))))%>%
  dplyr::select(-ci_data, -boot_08, -boot_09, -boot_10)%>%#, RE_ngml, RE_chol)%>%
  unnest(cols = c(ci_data08, ci_data09, ci_data10))

temp <- data.frame(country = temp$country,
           rebound = c(round(temp$ci_data08[,3], 2), round(temp$ci_data09[,3], 2), round(temp$ci_data10[,3], 2))*100,
           lower = c(round(temp$ci_data08[,1], 2), round(temp$ci_data09[,1], 2), round(temp$ci_data10[,1], 2))*100,
           upper = c(round(temp$ci_data08[,2], 2), round(temp$ci_data09[,2], 2), round(temp$ci_data10[,2], 2))*100)%>%
  mutate(month = rep(1:96, 3),
         start_year = rep(2008:2010, each = 480))%>%
  filter(month %in% c(seq(4,24,2)))

p_bars <- temp %>%
  ggplot(., aes(x=reorder(month, rebound), y = rebound, group = as.factor(start_year), col = as.factor(start_year)))+
  geom_ribbon(aes(ymin = lower, ymax = upper, fill = as.factor(start_year), col = NA ), alpha = 0.2)+
  geom_point(size = 3)+
  geom_hline(yintercept = 100, col = "red", linetype="dashed")+
  scale_fill_viridis_d("", alpha = 0.8)+
  scale_color_viridis_d("Starting year of the analysis")+#, begin = 0.2, end = 0.9)+
#  scale_fill_viridis_d("")+ #, begin = 0.2, end = 0.9)+
  # geom_errorbar(aes(ymin = lower, ymax = upper),  col = "black", 
  #               position = position_dodge2(width = 0.5, padding = 0.5), size = 1, stat = "identity")+
  ylab("Rebound effect in percent")+
  xlab("Time (Months)")+
  theme_bw()+
  geom_line(linetype = "dashed")+
  facet_wrap(~country)+
  theme(legend.position = c(0.8, 0.2))

pdf(paste0(savepath, "fig_e17_time_rebound.pdf"), width = 10, height = 7)
p_bars
dev.off()

# Explain difference between 08 and 09

uk_ts <- favars$svar08$`United Kingdom`
ge_ts <- favars$svar08$Germany
fr_ts <- favars$svar08$France
it_ts <- favars$svar08$Italy
us_ts <- favars$svar08$`United States`

# Explain influence of F1 and F2 for three exemplary countries
cf_tab <- svars::cf(us_ts, series = 1)
us_cf <- data.frame(time = seq.Date(from = as.Date(ymd('2008-01-01') %m+% months(us_ts$p)), to = as.Date('2019-09-01'), by = "month"),
                    actual = cf_tab$actual[,2], counter = cf_tab$counter[,2] -
                      (cf_tab$actual[,2] - cf_tab$counter[,3]) - (cf_tab$actual[,2] - cf_tab$counter[,4]))%>%
  gather(key = "type", value = "value", -time) 
cf_tab <- svars::cf(fr_ts, series = 1)
fr_cf <- data.frame(time = seq.Date(from = as.Date(ymd('2008-01-01') %m+% months(fr_ts$p)), to = as.Date('2019-09-01'), by = "month"),
                    actual = cf_tab$actual[,2], counter = cf_tab$actual[,2] -
                      (cf_tab$actual[,2] - cf_tab$counter[,3]) - (cf_tab$actual[,2] - cf_tab$counter[,4]))%>%
  gather(key = "type", value = "value", -time) 
cf_tab <- svars::cf(uk_ts, series = 1)
uk_cf <- data.frame(time = seq.Date(from = as.Date(ymd('2008-01-01') %m+% months(uk_ts$p)), to = as.Date('2019-09-01'), by = "month"),
                    actual = cf_tab$actual[,2], counter = cf_tab$actual[,2] -
                      (cf_tab$actual[,2] - cf_tab$counter[,3]) - (cf_tab$actual[,2] - cf_tab$counter[,4]))%>%
  gather(key = "type", value = "value", -time) 
cf_tab <- svars::cf(ge_ts, series = 1)
ge_cf <- data.frame(time = seq.Date(from = as.Date(ymd('2008-01-01') %m+% months(ge_ts$p)), to = as.Date('2019-09-01'), by = "month"),
                    actual = cf_tab$actual[,2], counter = cf_tab$actual[,2] -
                      (cf_tab$actual[,2] - cf_tab$counter[,3]) - (cf_tab$actual[,2] - cf_tab$counter[,4]))%>%
  gather(key = "type", value = "value", -time) 
cf_tab <- svars::cf(it_ts, series = 1)
it_cf <- data.frame(time = seq.Date(from = as.Date(ymd('2008-01-01') %m+% months(it_ts$p)), to = as.Date('2019-09-01'), by = "month"),
                    actual = cf_tab$actual[,2], counter = cf_tab$actual[,2] -
                      (cf_tab$actual[,2] - cf_tab$counter[,3]) - (cf_tab$actual[,2] - cf_tab$counter[,4]))%>%
  gather(key = "type", value = "value", -time) 

p_cf <- bind_rows(list(UK = uk_cf, US =us_cf, Germany = ge_cf, Italy = it_cf, France = fr_cf), .id = "country")%>%
  ggplot(aes(x = time, col = type, lty = type, y = value))+
  geom_line()+
  facet_wrap(~country, scales = "free")+
  theme_bw()+
  theme(legend.position = c(0.9, 0.2))

ggsave(paste0(savepath, "cf_without_factors.pdf"), p_cf, width = 8, height = 5)



pdf(paste0(savepath, "hist_dec_all_bars.pdf"), width = 10, height = 8)
plot_grid(
  p1 = plot_sel_hd(x = (uk_ts),
                   begin = ymd('2008-01-01') %m+% months(uk_ts$p),
                   end = ymd('2008-12-01'), 
                   date_of_interst_start = ymd('2008-05-01')
  ), plot_grid(plotlist = list(
  p2 = plot_sel_hd(x = (ge_ts),
            begin = ymd('2008-01-01') %m+% months(ge_ts$p),
            end = ymd('2008-12-01'), 
            date_of_interst_start = ymd('2008-05-01')
            )+
  theme(legend.position = "none"),
p3 = plot_sel_hd(x = (fr_ts),
            begin = ymd('2008-01-01') %m+% months(fr_ts$p),
            end = ymd('2008-12-01'), 
            date_of_interst_start = ymd('2008-05-01'))+
  theme(legend.position = "none"),
p4 = plot_sel_hd(x = (it_ts),
            begin = ymd('2008-01-01') %m+% months(it_ts$p),
            end = ymd('2008-12-01'), 
            date_of_interst_start = ymd('2008-05-01'))+
  theme(legend.position = "none"),
p5 = plot_sel_hd(x = (us_ts),
            begin = ymd('2008-01-01') %m+% months(us_ts$p),
            end = ymd('2008-12-01'), 
            date_of_interst_start = ymd('2008-05-01'))+
  theme(legend.position = "none")
), 
ncol = 2, labels = c("Germany,", "France", "Italy", "US"), label_y = 1.1), ncol = 1, labels = c("UK", ""), label_y = 1)
dev.off()



