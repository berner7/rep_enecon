#------------------------------------
# This code produces the results for 
#	the comparison between the quarterly
# GDP series and the filtered/interpolated
# series
#------------------------------------

#-----------------------------------
# Packages
#-----------------------------------
library(dplyr)
library(ggplot2)
library(zoo)
library(tidyr)
library(fredr)
# Set API
fredr_set_key("c02957c64fb8b763a4fea09ada56877a")

#---------------------------------------
# Paths
#---------------------------------------
loadpath <- loadpath #change this
savepath <- "Results/" #change this


# GDP Series (OECD)
Sys.setlocale("LC_TIME", 'en_GB.UTF-8')
temp <- list.files(path = loadpath, pattern = "gdp_filtered")
gdp <- read.table(file = paste0(loadpath, temp[[1]]), header = TRUE)[,-2] %>%
  mutate(Time = as.yearmon(as.character(format(Time, nsmall = 2)), format = "%Y.%m"),
         Id = "Filtered",
         DEU = read.table(file = paste0(loadpath, temp[[1]]), header = TRUE)[,3],
         FRA = read.table(file = paste0(loadpath, temp[[2]]), header = TRUE)[,3],
         GBR = read.table(file = paste0(loadpath, temp[[3]]), header = TRUE)[,3],
         ITA = read.table(file = paste0(loadpath, temp[[4]]), header = TRUE)[,3]
  )%>%
  dplyr::select(.,-Smoothed_mly_GDP)%>%
  gather(key = "LOCATION", value = "Value", -Time, -Id)

temp <- list.files(path = loadpath, pattern = "gdp_ip_filtered")
gdp_ip <- read.table(file = paste0(loadpath, temp[[1]]), header = TRUE)[,-2] %>%
  mutate(Time = as.yearmon(as.character(format(Time, nsmall = 2)), format = "%Y.%m"),
         Germa = read.table(file = paste0(loadpath, temp[[1]]), header = TRUE)[,3],
         Franc = read.table(file = paste0(loadpath, temp[[2]]), header = TRUE)[,3],
         Unite = read.table(file = paste0(loadpath, temp[[3]]), header = TRUE)[,3],
         Italy = read.table(file = paste0(loadpath, temp[[4]]), header = TRUE)[,3]
  )%>%
  dplyr::select(.,-Smoothed_mly_GDP)

# Original Data

gdp_q <- data.frame(GBR = fredr(series_id = "CLVMNACSCAB1GQUK", observation_start = as.Date("2000-01-01"), 
                                observation_end = as.Date("2019-11-01"))[,c("date", "value")],
                    GER = fredr(series_id = "CLVMNACSCAB1GQDE", observation_start = as.Date("2000-01-01"), 
                                observation_end = as.Date("2019-11-01"))[,"value"],
                    ITA = fredr(series_id = "CLVMNACSCAB1GQIT", observation_start = as.Date("2000-01-01"), 
                                observation_end = as.Date("2019-11-01"))[,"value"],
                    FRA = fredr(series_id = "CLVMNACSCAB1GQFR", observation_start = as.Date("2000-01-01"), 
                                observation_end = as.Date("2019-11-01"))[,"value"]
)

colnames(gdp_q) <- c("Time", "GBR", "DEU", "ITA", "FRA")
Zeit <- data.frame(Time = seq.Date(as.Date("2000/1/1"), as.Date("2019/12/1"), by = "month"))
gdp_q <- gdp_q %>%
  right_join(Zeit, by = "Time") %>%
  mutate(Id = c("GDP"),
         Time = as.yearmon(Time)) %>%
  filter(Time >= "Jan 2000" & Time <= "Nov 2019") %>%
  tidyr::gather("LOCATION", "Value", -Time, -Id)


# IPI
temp <- data.frame(GBR = fredr(series_id = "GBRPROINDMISMEI", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,c("date", "value")],
                   GER = fredr(series_id = "DEUPROINDMISMEI", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,"value"],
                   ITA = fredr(series_id = "ITAPROINDMISMEI", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,"value"],
                   FRA = fredr(series_id = "FRAPROINDMISMEI", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,"value"])
colnames(temp) <- c("Time", "GBR", "DEU", "ITA", "FRA")
ipi <- temp %>%
  mutate(Time = as.yearmon(Time),
         Id = c("IP")) %>%
  filter(Time >= "Jan 2000" & Time <= "Nov 2019") %>%
  tidyr::gather("LOCATION", "Value", -Time, -Id)


# CPI
temp <- data.frame(GBR = fredr(series_id = "GBRCPIALLMINMEI", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,c("date", "value")],
                   GER = fredr(series_id = "DEUCPIALLMINMEI", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,"value"],
                   ITA = fredr(series_id = "ITACPIALLMINMEI", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,"value"],
                   FRA = fredr(series_id = "FRACPIALLMINMEI", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,"value"])
colnames(temp) <- c("Time", "GBR", "DEU", "ITA", "FRA")
cpi <- temp %>%
  mutate(Time = as.yearmon(Time),
         Id = c("CPI")) %>%
  filter(Time >= "Jan 2000" & Time <= "Dec 2019") %>%
  tidyr::gather("LOCATION", "Value", -Time, -Id)

# Retail Trade
temp <- data.frame(GBR = fredr(series_id = "GBRSARTMISMEI", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,c("date", "value")],
                   GER = fredr(series_id = "DEUSARTMISMEI", observation_start = as.Date("2000-01-01"),
                               observation_end = as.Date("2019-11-01"))[,"value"], 
                   ITA = fredr(series_id = "ITASARTMISMEI", observation_start = as.Date("2000-01-01"),
                               observation_end = as.Date("2019-11-01"))[,"value"],
                   FRA = fredr(series_id = "FRASARTMISMEI", observation_start = as.Date("2000-01-01"),
                               observation_end = as.Date("2019-11-01"))[,"value"])
colnames(temp) <- c("Time", "GBR", "DEU", "ITA", "FRA")

rt <- temp %>%
  mutate(Time = as.yearmon(Time),
         Id = c("RT")) %>%
  filter(Time >= "Jan 2000" & Time <= "Nov 2019") %>%
  tidyr::gather("LOCATION", "Value", -Time, -Id) %>%
  dplyr::group_by(LOCATION)%>%
  dplyr::mutate(Value = Value/first(Value)*100)%>%
  as_tibble()


# Unemployment Rate

temp <- data.frame(GBR = fredr(series_id = "LRHUTTTTGBM156S", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,c("date", "value")],
                   GER = fredr(series_id = "LRHUTTTTDEM156S", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,"value"],
                   ITA = fredr(series_id = "LRHUTTTTITM156S", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,"value"],
                   FRA = fredr(series_id = "LRHUTTTTFRM156S", observation_start = as.Date("2000-01-01"), 
                               observation_end = as.Date("2019-11-01"))[,"value"])
colnames(temp) <- c("Time", "GBR", "DEU", "ITA", "FRA")

empl <- temp %>%
  mutate(Time = as.yearmon(Time),
         Id = c("EMP")) %>%
  filter(Time >= "Jan 2000" & Time <= "Nov 2019") %>%
  tidyr::gather("LOCATION", "Value", -Time, -Id)%>%
  dplyr::group_by(LOCATION)%>%
  dplyr::mutate(Value = Value/first(Value)*100)%>%
  as_tibble()

# Combine both in 1 data frame for plotting
ipi_m <- ipi %>%
  dplyr::select(Time, Id, LOCATION, Value)
gdp_m <- gdp_q %>%
  filter(Time >= "Jan 2000" & Time <= "Nov 2019" )%>%
  dplyr::group_by(LOCATION)%>%
  dplyr::mutate(Value = Value/first(Value)*100)%>%
  as_tibble()

gdp_i <- gdp %>%
  dplyr::group_by(LOCATION)%>%
  dplyr::mutate(Value = Value/first(Value)*100)%>%
  as_tibble()

cpi_m <- cpi %>%
  dplyr::select(Time, Id, LOCATION, Value)

p1_all <- rbind(gdp_i, ipi_m, cpi, empl, rt, gdp_m)%>%
  filter(Time >= "Feb 2008" & Time <= "Jan 2019" )%>%
  spread(key = Id, value = Value)%>%
  mutate(LOCATION = factor(LOCATION, labels = c("Germany", "France", "UK", "Italy")))%>%
  ggplot()+
  geom_line(aes(y = EMP, x = Time, col = "EMPL"), col = "grey")+
  geom_line(aes(y = CPI, x = Time, col = "CPI"), col = "grey")+
  geom_line(aes(y = IP, x = Time, col = "IP"), col = "grey")+
  
  geom_line(aes(y = Filtered, x = Time, col = "Filtered Values"), size = 1)+
  geom_point(aes(x = Time, y = GDP, col = "GDP"))+
  facet_wrap(~LOCATION, scales = "free_y")+
  scale_x_yearmon(format = "%Y")+
  scale_y_continuous("Index [2000=100]")+
  theme_bw()+
  xlab("")+
  scale_color_discrete("")+
  theme(legend.position = "top", legend.justification = "right")
  
ggsave(filename = paste0(savepath, "figa8_kalman_ts_all_countries.pdf"), p1_all, width = 10, height = 10)

p1_ger <- rbind(gdp_i, ipi_m, cpi, empl, rt, gdp_m)%>%
  filter(Time >= "Feb 2008" & Time <= "Jan 2019" & LOCATION == "DEU")%>%
  spread(key = Id, value = Value)%>%
  ggplot()+
  geom_line(aes(y = EMP, x = Time, col = "EMPL"), col = "grey")+
  geom_line(aes(y = CPI, x = Time, col = "CPI"), col = "grey")+
  geom_line(aes(y = IP, x = Time, col = "IP"), col = "grey")+
  geom_line(aes(y = Filtered, x = Time, col = "Filtered Values"), size = 1)+
  geom_point(aes(x = Time, y = GDP, col = "GDP"))+
  scale_x_yearmon(format = "%Y")+
  scale_y_continuous("Index [2000=100]")+
  theme_bw()+
  xlab("")+
  scale_color_discrete("")+
  theme(legend.position = "top", legend.justification = "right")

pdf(file = "fig_a7_kalman_ts_germany.pdf", width = 8, height = 4)
p1_ger
dev.off()
