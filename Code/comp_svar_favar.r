#------------------------------------
# This code produces the results for 
#	the S-FAVAR using two factors
#------------------------------------

#-----------------------------------
# Packages
#-----------------------------------
library(dplyr); 
library(tidyr);  
library(zoo); 
library(ggplot2); 
library(plyr); 
library(purrr)
library(vars)
library(svars)
library(xtable)
library(normtest)

#---------------------------------------
# Paths
#---------------------------------------
loadpath <- "Data/" #change this
savepath <- "Results/" #change this

#---------------------------------------
# Data
#---------------------------------------
# monthly data
favar_dat2 <- readRDS(paste(loadpath, "favar_data_2factors.rds", sep=""))

#---------------------------------------
# Functions
#---------------------------------------
# (1) Gauss tests  
source("Code/Functions/gauss_tests.r")
# (2) Reduced-form estimation 
source("Code/Functions/var_prep.r")
# (3) FEVD decomposition plot 
source("Code/Functions/fevd_plot.r")
# (4) Transform bootstrap data to rebound effect (including confidence bans) and plot effect
source("Code/Functions/boot_eu.r")
# (5) Plot IRF of efficiency shock for comparing identification strategies
source("Code/Functions/plot_identif_comp.r")
# (6) Confidence Intervals for estimated energy efficiency shock and IRF 
source("Code/Functions/confidence_re.r")
# (7) Plot all IRF for comparing identification strategies 
source("Code/Functions/plot_boot_comp.r")
# (8) Transform bootstrap data to rebound effect (including confidence bans) and plot effect
source("Code/Functions/plot_re.r")
#---------------------------------------
# Estimation of reduced form and Gauss Tests
#---------------------------------------

# Estimate reduced-form FAVARs (compare lag.max = 6 and 12)
favar2008 <- favar_dat2 %>%
  dplyr::select(country, data = fac_est)%>%
  mutate(var6 = map(data, var_prep, log = TRUE))%>%
  mutate(red_form = map(var6, "vars"))%>%
  mutate(data = map(data, ~dplyr::select(.x,time, E, Y, P)))%>%
  mutate(var6 = map(data, var_prep, log = TRUE))%>%
  mutate(red_form_svar = map(var6, "vars"))


# S-FAVAR estimation with different identification methods (dcov, st, chol, ngml)
favars <- favar2008%>%
  dplyr::select(country, red_form, red_form_svar)%>%
  mutate(favar = map(red_form, id.dc))%>%
  mutate(svar = map(red_form_svar, id.dc))


# Bootstrap 
set.seed(42)
favars <- favars %>% mutate(
  boot_fav = map(favar, boot_eu),
  boot_svar = map(svar, boot_eu)
)

saveRDS(favars, paste0(savepath, "comp_svar_favar_boot.rds"))
favars <- readRDS(paste0(savepath, "comp_svar_favar_boot.rds"))

# Impulse-response function plots
#-------------------------------------------------------------------------
# Rebound effect calculation
#-------------------------------------------------------------------------
### Barplot (Figure 6) 
temp <- favars %>% 
  dplyr::select(country, boot_fav, boot_svar)%>%
  mutate(ci_data =map(map(boot_fav, "bootstrap"), confidence_re, lowerq = 0.1, upperq = 0.9))%>%
  mutate(ci_data_fav = map(ci_data, function(x) apply(x, 2, function(x) 1 - (x/x[1]))))%>%
  mutate(ci_data =map(map(boot_svar, "bootstrap"), confidence_re, lowerq = 0.1, upperq = 0.9))%>%
  mutate(ci_data_sv = map(ci_data, function(x) apply(x, 2, function(x) 1 - (x/x[1]))))%>%
  dplyr::select(-ci_data, -boot_fav, -boot_svar)%>%#, RE_ngml, RE_chol)%>%
  unnest(cols = c(ci_data_sv, ci_data_fav))

temp <- data.frame(country = temp$country,
                   rebound = c(round(temp$ci_data_sv[,3], 2), round(temp$ci_data_fav[,3], 2))*100,
                   lower = c(round(temp$ci_data_sv[,1], 2), round(temp$ci_data_fav[,1], 2))*100,
                   upper = c(round(temp$ci_data_sv[,2], 2), round(temp$ci_data_fav[,2], 2))*100)%>%
  mutate(month = rep(1:96, 2),
         model = rep(c("svar", "favar"), each = 480))%>%
  filter(month %in% c(6, 12, 24))

p_bars <- temp %>%
  ggplot(., aes(x=reorder(month, rebound),ymin = lower, ymax = upper, y = rebound, group = as.factor(model), fill = as.factor(model)))+
  geom_bar(stat='identity', position='dodge')+
  geom_hline(yintercept = 100, col = "red", linetype="dashed")+
  geom_errorbar(aes(ymin = lower, ymax = upper),  col = "black", position = position_dodge2(width = 0.5, padding = 0.5), size = 1, stat = "identity")+
  scale_color_viridis_d("Model of the analysis", begin = 0.2, end = 0.9)+
  scale_fill_viridis_d("", begin = 0.2, end = 0.9)+
  ylab("Rebound effect in percent")+
  xlab("Time (Months)")+
  theme_bw()+
  facet_wrap(~country)+
  theme(legend.position = c(0.8, 0.2))

pdf(paste0(savepath, "fig_svar_vs_favar_rebound.pdf"), width = 8, height = 6)
p_bars
dev.off()

