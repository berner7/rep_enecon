#------------------------------------
# This code produces the results for 
#	the S-FAVAR using two factors
#------------------------------------

#-----------------------------------
# Packages
#-----------------------------------
library(dplyr); 
library(tidyr);  
library(zoo); 
library(ggplot2); 
library(plyr); 
library(purrr)
library(vars)
library(svars)
library(xtable)
library(normtest)

#---------------------------------------
# Paths
#---------------------------------------
loadpath <- "Data/" #change this
savepath <- "Results/" #change this

#---------------------------------------
# Data
#---------------------------------------
# monthly data
favar_dat2 <- readRDS(paste(loadpath, "favar_data_2factors.rds", sep=""))

#---------------------------------------
# Functions
#---------------------------------------
# (1) Gauss tests  
source("Code/Functions/gauss_tests.r")
# (2) Reduced-form estimation 
source("Code/Functions/var_prep.r")
# (3) FEVD decomposition plot 
source("Code/Functions/fevd_plot.r")
# (4) Transform bootstrap data to rebound effect (including confidence bans) and plot effect
source("Code/Functions/boot_eu.r")
# (5) Plot IRF of efficiency shock for comparing identification strategies
source("Code/Functions/plot_identif_comp.r")
# (6) Confidence Intervals for estimated energy efficiency shock and IRF 
source("Code/Functions/confidence_re.r")
# (7) Plot all IRF for comparing identification strategies 
source("Code/Functions/plot_boot_comp.r")
# (8) Transform bootstrap data to rebound effect (including confidence bans) and plot effect
source("Code/Functions/plot_re.r")
#---------------------------------------
# Estimation of reduced form and Gauss Tests
#---------------------------------------

# Estimate reduced-form FAVARs (compare lag.max = 6 and 12)
favar2 <- favar_dat2 %>%
  dplyr::select(country, data = fac_est)%>%
  mutate(var6 = map(data, var_prep, log = TRUE),
         var12 = map(data, var_prep, log = TRUE))%>%
  mutate(p6 = map(var6, "p"),
         p12 = map(var12, "p"),
         gauss6 = map(map(var6, "res"), gauss_tests),
         gauss12 = map(map(var12,"res"), gauss_tests),
         red_form6 = map(var6, "vars"),
         red_form12 = map(var12, "vars"))

# Comparison of maximum lag length (Table E.6)
write.csv(favar2%>%
            dplyr::select(country, p6, p12)%>%
            unnest(cols = c(p6, p12)), file = paste(savepath, "tab_e6_laglength_comp.csv",sep = ""))

# Test for normality (Table D.5)
write.csv(favar2%>%
         dplyr::select(country, gauss6)%>%
         unnest(cols = c(gauss6)), file = paste(savepath, "tab_d5_normailty_tests.csv",sep = ""))


# S-FAVAR estimation with different identification methods (dcov, st, chol, ngml)
favars <- favar2%>%
  dplyr::select(country, red_form6)%>%
  mutate(dc = map(red_form6, id.dc),
         st = map(red_form6, id.st), # takes long time to compute
         chol = map(red_form6, id.chol, order_k = c("Y", "E", "P", "F1", "F2")),
         ngml = map(red_form6, id.ngml)
  )


# B matrices
B <- favars  %>% 
  mutate(B_dc = map(dc, "B"), B_ngml = map(ngml, "B"), B_chol = map(chol, "B")) %>% 
  dplyr::select(country, B_dc, B_ngml, B_chol)

# FEVD plots for dcov
fevd_list <- map(favars$dc, fevd_plot)
legend <- get_legend(fevd_list[[1]])
t1 <- cowplot::plot_grid(fevd_list[[1]]+ theme(legend.position="none") + xlab("")+ ylab(""),
                         fevd_list[[2]] + theme(legend.position="none") + xlab("")+ ylab(""),
                         fevd_list[[3]] + theme(legend.position="none")+ xlab(""),
                         fevd_list[[4]] + theme(legend.position="none") + ylab("")+ xlab(""),
                         fevd_list[[5]] + theme(legend.position="none") + ylab(""),
                         ncol = 1, labels = c("Germany","Italy", "UK", "France", "USA"), label_y = 1.09)
plot1 <- cowplot::plot_grid(legend, t1, ncol = 1, rel_heights = c(0.05, 0.95)) 
pdf(file = paste0(savepath, "fig_5_b9_fevd.pdf"), width = 8, height = 12)
plot1
dev.off()

# Bootstrap 
set.seed(42)
favars <- favars %>% mutate(
  boot_ngml = map(ngml, boot_eu),
  boot_dc = map(dc, boot_eu),
  boot_chol = map(chol, boot_eu), 
  boot_st = map(st, boot_eu)
)
saveRDS(favars, paste0(savepath, "favars_boot.rds"))
favars <- readRDS(paste0(savepath, "favars_boot.rds"))

# B for dcov with confidence intervals
x <- favars$boot_dc
table_all <- list()
for (i in names(x)){
  boot_mean <- round(matrix(
    unlist(x[[i]]$bootstrap$true$irf[1, ])[-1],
    nrow = 5, 
    byrow = TRUE), 2)
  boot_low <- apply(simplify2array(x[[i]]$bootstrap$boot_B), 1:2,
                    function(x) round(quantile(x, 0.05), 2))
  boot_high <- apply(simplify2array(x[[i]]$bootstrap$boot_B), 1:2,
                     function(x) round(quantile(x, 0.95), 2))
  table <- list()
  for (j in 1:5) {
    mean <- boot_mean[j, ]
    ci <- paste0("(", boot_low[j, ], ", ", boot_high[j, ], ")")
    table[[j]] <- data.frame(rbind(mean, ci))
  }
  temp <- bind_rows(table)
  colnames(temp) <- c("E", "Y", "P", "F1", "F2")
  rownames(temp) <- c("e_t", "1", "y_t", "2", "p_t", "3", "F1_t",
                      "4", "F2_t", "5")
  table_all[[i]] <- temp
}

# Table C.4 (Contemporaneous reaction of the variables to different shocks)
write.csv(table_all, paste0(savepath, "tab_2_all_B_ci.csv"))

# Impulse-response function plots
favar_plots <- favars %>% 
  mutate(plots = lapply(map(favars$boot_dc,"bootstrap"), plot, lowerq = 0.1, upperq = 0.9))

favar_plots %>% 
  dplyr::select(country, plots)%>%
  pmap(function(country, plots){
    ggsave(filename = paste0(savepath, "fig_c_irf_favars2_dc", "_",
                             country, ".pdf"), plots+xlim(0,24), width = 8, height = 6)})


# Comparison of identification methods for the efficiency shock (Figure E.19)
favar_plots <- favars %>%
  mutate(dc = map(boot_dc, "bootstrap"),
#         st = map(boot_st, "bootstrap"),
         chol = map(boot_chol, "bootstrap"),
         ngml = map(boot_ngml, "bootstrap"))%>%
  dplyr::select(country, dc, ngml, chol)

pdf(file = paste0(savepath, "fig_e19_irf_ident.pdf"), width = 12, height = 8)
plot_identif_comp(favar_plots)
dev.off()

# Comparison of identification methods for all IRFs (Figure C10) 
favar_plots <- favars %>%
  mutate(dc = map(boot_dc, "bootstrap"))%>%
  dplyr::select(country, dc)
p2 <- plot_boot_comp(favar_plots, lowerq = 0.16, upperq = 0.84)+xlim(0,24)

ggsave(paste0(savepath, "fig_c10_comp_irf_all.pdf"), p2, width = 20, height = 12)


#-------------------------------------------------------------------------
# Rebound effect calculation
#-------------------------------------------------------------------------
# Plot rebound effect
re_plot <- favars %>% dplyr::select(country, boot_dc) %>%
  mutate(boot_dc = map(boot_dc, "bootstrap")) %>%
  group_nest()%>%
  mutate(plots = map(data, plot_re))

### Barplot (Figure 6) 
temp <- favars %>% 
  dplyr::select(country, boot_dc)%>%
  mutate(ci_data =map(map(boot_dc, "bootstrap"), confidence_re, lowerq = 0.1, upperq = 0.9))%>%
  mutate(ci_data = map(ci_data, function(x) apply(x, 2, function(x) 1 - (x/x[1]))))%>%
  dplyr::select(-boot_dc)%>%#, RE_ngml, RE_chol)%>%
  unnest(cols = c(ci_data))%>%
  mutate(rebound = round(ci_data[,3], 2)*100,
         lower = round(ci_data[,1], 2)*100,
         upper = round(ci_data[,2], 2)*100
  )%>%
  dplyr::select(-ci_data)%>%
  mutate(month = rep(c(1:96), 5))%>%
  filter(month %in% c(4, 6, 12, 24))

p_bars <- temp %>%
  ggplot(., aes(x=reorder(month, rebound), y = rebound, fill = reorder(country, rebound)))+
  geom_bar(position = "dodge", stat = "identity")+
  scale_fill_viridis_d("", alpha = 0.8)+
  scale_color_viridis_d("", begin = 0.2, end = 0.9)+
  geom_errorbar(aes(ymin = lower, ymax = upper),  col = "black", position = position_dodge2(width = 0.5, padding = 0.5), size = 1, stat = "identity")+
  ylab("Rebound effect in percent")+
  xlab("Time (Months)")+
  theme_bw()+
  geom_hline(yintercept = 100, col = "red", linetype="dashed")

pdf(paste0(savepath, "fig_6_bars_rebound.pdf"), width = 5, height = 3)
p_bars
dev.off()

re_mean_plot <- favars %>% dplyr::select(country, boot_dc) %>%
  mutate(boot_dc = map(boot_dc, "bootstrap", lowerq = 0.1, upperq = 0.9)) %>%
  group_nest()%>%
  mutate(plot = map(data, plot_re, mean_est = TRUE))

pdf(paste0(savepath, "fig_6_irf_rebound.pdf"), width = 5, height = 3)
 re_mean_plot$plot
dev.off()

# Table for rebound effects for different id 
temp <- favars %>% 
  filter(model == "model1")%>%
  mutate(months = list(c(1:96)),
         dc = map(boot_dc, "RE"),
         chol = map(boot_chol, "RE"),
         ngml = map(boot_ngml, "RE")
  )%>%
  dplyr::select(country, months, dc, ngml, chol)%>%
  unnest(.)%>%
  filter(months %in% c(3, 6, 12, 24))
print(xtable(temp),  include.rownames=FALSE)



pdf(paste(savepath, "fig_3_est_latent_factors.pdf",sep = ""), width = 8, height = 4)
  factor_plots
dev.off()


