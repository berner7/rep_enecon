#------------------------------------
# This code produces the results for 
#	the comparison between the original 
# rebound and the rebound including electricity use
#------------------------------------

#-----------------------------------
# Packages
#-----------------------------------
library(dplyr); 
library(tidyr);  
library(zoo); 
library(ggplot2); 
library(plyr); 
library(purrr)
library(vars)
library(svars)
library(xtable)
library(normtest)

#---------------------------------------
# Paths
#---------------------------------------
loadpath <- "Results/Elec/" #change this
savepath <- "Results/Elec/" #change this

#---------------------------------------
# Data
#---------------------------------------
# bootstrapped data
favar_elec <- readRDS(paste(loadpath, "favars_boot.rds", sep=""))
favar_fuel <- readRDS(paste("temp/favars_time_dep.rds", sep=""))

#---------------------------------------
# Functions
#---------------------------------------
# (1) Comp identification methods
source("Code/Functions/plot_identif_comp.r")
# (6) Confidence Intervals for estimated energy efficiency shock and IRF 
source("Code/Functions/confidence_re.r")
# (7) Plot all IRF for comparing identification strategies 
source("Code/Functions/plot_boot_comp.r")
# (8) Transform bootstrap data to rebound effect (including confidence bans) and plot effect
source("Code/Functions/plot_re.r")

# Bootstrap 
set.seed(42)
favars <- tibble(country = favar_elec$country, boot_elec = favar_elec$boot_dc, boot_fuel = favar_fuel$boot_10)

#-------------------------------------------------------------------------
# Rebound effect calculation
#-------------------------------------------------------------------------
### Barplot (Figure 6) 
temp <- favars %>% 
  mutate(ci_data =map(map(boot_fuel, "bootstrap"), confidence_re, lowerq = 0.1, upperq = 0.9))%>%
  mutate(ci_data_fuel = map(ci_data, function(x) apply(x, 2, function(x) 1 - (x/x[1]))))%>%
  mutate(ci_data =map(map(boot_elec, "bootstrap"), confidence_re, lowerq = 0.1, upperq = 0.9))%>%
  mutate(ci_data_elec = map(ci_data, function(x) apply(x, 2, function(x) 1 - (x/x[1]))))%>%
  dplyr::select(-ci_data, -boot_elec, -boot_fuel)%>%#, RE_ngml, RE_chol)%>%
  unnest(cols = c(ci_data_fuel, ci_data_elec))

temp <- data.frame(country = temp$country,
                   rebound = c(round(temp$ci_data_fuel[,3], 2), round(temp$ci_data_elec[,3], 2))*100,
                   lower = c(round(temp$ci_data_fuel[,1], 2), round(temp$ci_data_elec[,1], 2))*100,
                   upper = c(round(temp$ci_data_fuel[,2], 2), round(temp$ci_data_elec[,2], 2))*100)%>%
  mutate(month = rep(1:96, 2),
         model = rep(c("fuels", "fuels_elec"), each = 480))%>%
  filter(month %in% c(seq(4,24,2)))

p_bars <- temp %>%
  ggplot(., aes(x=reorder(month, rebound), y = rebound, group = as.factor(model), col = as.factor(model)))+
  geom_ribbon(aes(ymin = lower, ymax = upper, fill = as.factor(model), col = NA ), alpha = 0.2)+
  geom_point(size = 3)+
  geom_hline(yintercept = 100, col = "red", linetype="dashed")+
  scale_color_viridis_d("Model of the analysis", begin = 0.2, end = 0.7)+
  scale_fill_viridis_d("", begin = 0.2, end = 0.8)+
  ylab("Rebound effect in percent")+
  xlab("Time (Months)")+
  theme_bw()+
  geom_line(linetype = "dashed")+
  facet_wrap(~country)+#, scales = "free")+
  theme(legend.position = c(0.8, 0.2))

pdf(paste0(savepath, "fig_g23_elec_vs_fuel_rebound.pdf"), width = 8, height = 6)
p_bars
dev.off()

