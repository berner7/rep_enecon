# Plot FEVD for list of svars combined
fevd_plot <- function(svar, forecast.length = 24){
  data <- fevd(svar, n.ahead= forecast.length)%>%
    unclass()%>%
    dplyr::bind_rows(., .id = "id")%>%
    mutate(id = revalue(id, c(E = "Energy Use", Y = "GDP", P = "Price")))
  
plot <- ggplot(data, aes(group = id, x = rep(1:forecast.length, length(unique(id)))))+
    geom_area(aes(y = E,
                  fill = "Energy")) +
    geom_ribbon(aes(ymin =  E,
                    ymax = E + Y,
                    fill = "GDP")) +
    geom_ribbon(aes(ymin =  E+ Y,
                    ymax = E+ Y + P,
                    fill = "Price"))+
    geom_ribbon(aes(ymin =  E+ Y + P,
                    ymax = E+ Y + P + F1,
                    fill = "F1"))+
    geom_ribbon(aes(ymin =  E+ Y + P + F1,
                  ymax = E+ Y + P + F1 +F2,
                  fill = "F2"))+
  # geom_ribbon(aes(ymin =  E+ Y + P + F1 + F2,
  #                 ymax = E+ Y + P + F1 +F2 + F3,
  #                 fill = "F3"))+
    facet_wrap(~id)+
    scale_fill_viridis_d( "Explained Variance by       ")+
    scale_x_continuous(expand = c(0, 0),limits = c(1,forecast.length)) +
    scale_y_continuous(expand = c(0, 0)) +
    theme_bw(base_size=11.5)  +
    labs(y="Explained Variance in Percent",
        x="Months") +
    theme(legend.position = "top", legend.justification = "right")
  # legend <- get_legend(fv_plot+ 
  #   guides(color = guide_legend(nrow = 1)) +
  #   theme(legend.position = "top", legend.justification = "right"))
  return(plot_fevd = plot )#, legend = legend))
}
