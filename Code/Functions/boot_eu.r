# Bootstrap the structural FAVAR model and calculate rebound (Input SVAR model)
boot_eu <- function(svar, n.ahead = 96){
  svar$B[,1] <- svar$B[,1]*-1
  boot_usf <- svars::wild.boot(svar, nboot = 1000, nc = 3, n.ahead = n.ahead)
  boot_PEV <- boot_usf[["true"]][["irf"]][["epsilon[ E ] %->% E"]]
  RE <- 1 - (boot_PEV/boot_PEV[1])
  
  return(list(bootstrap = boot_usf, PEV = boot_PEV, RE = RE))
}


