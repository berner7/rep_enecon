# Confidence Intervals for estimated energy efficiency shock and IRF 
confidence_re <- function(x, scales = "free_y", lowerq = 0.1, upperq = 0.9, 
                          percentile = 'standard'){
  impulse <- x$true$irf$`epsilon[ E ] %->% E`
#  impulse <- (1-(impulse/impulse[1]))*100
  confidence <- lapply(lapply(x$bootstrap, "[[", "irf"), apply, 2, function(x) x)#{(1-(x/x[1]))*100})
                       
  horizon <- nrow(confidence[[1]])
  nboot <- length(confidence)
  
  intervals <- array(0, c(horizon, nboot))
  for(i in 1:nboot){
    intervals[,i] <- as.matrix(confidence[[i]][,2])
  }
  
  lower <- matrix(0, horizon)
  for(i in 1:horizon){
    lower[i] <- quantile(intervals[i,], probs = lowerq)
  }
  
  upper <- matrix(0, horizon)
  for(i in 1:horizon){
    upper[i] <- quantile(intervals[i,], probs = upperq)
  }
  data_ci <- data.frame(lower = lower, upper = upper, impulse = impulse)*100
                   

  
  return(data_ci)
}


