# Plot all IRF for comparing identification strategies (including confidence intervals)
plot_boot_comp <- function(boots, scales = "free_y", lowerq = 0.05, upperq = 0.95, ..., base){
  
  # Set names to boot objects
  if(length(boots) == 1){data <- boots[[1]]
    }else{
      data <- boots[[2]]
    }
  
  
  n <- length(data)
  
  cbs <- list()
  imp <- list()
  
  for(e in 1:n){
    x <- data[[e]]
    # define
    probs <- NULL
    V1 <- NULL
    value <- NULL
    
    n.ahead <- nrow(x$true$irf)
    kk <- ncol(x$true$irf)
    bootstrap <- x$bootstrap
    nboot <- length(bootstrap)
    rest <- x$rest_mat
    
    n.probs <- length(lowerq)
    intervals <- array(0, c(n.ahead, kk, nboot))
    for(i in 1:nboot){
      intervals[,,i] <- as.matrix(bootstrap[[i]]$irf)
    }
    
    # find quantiles for lower and upper bounds
    lower <- array(0, dim = c(n.ahead, kk, n.probs))
    upper <- array(0, dim = c(n.ahead, kk, n.probs))
   for(i in 1:n.ahead){
        for(j in 1:kk){
          lower[i,j, ] <- quantile(intervals[i,j, ], probs = lowerq)
          upper[i,j, ] <- quantile(intervals[i,j, ], probs = upperq)
        }
   }
    alp <- 0.6 * (1+log(n.probs, 10))/n.probs
    irf <- melt(x$true$irf, id = 'V1')
    cbs[[e]] <- data.frame(V1 = rep(irf$V1, times=n.probs),
                      variable = rep(irf$variable, times=n.probs),
                      probs = rep(1:n.probs, each=(kk-1)*n.ahead),
                      lower = c(lower[,-1,]),
                      upper = c(upper[,-1,])) 
    imp[[e]] <- irf
  }
  cbs <- set_names(cbs, boots$country)%>%
    bind_rows(., .id = "country")
  
  irf <- set_names(imp, boots$country)%>%
    bind_rows(., .id = "country")
  # plot IRF with confidence bands
  ggplot() +
    geom_ribbon(data=cbs, aes(x=V1, ymin=lower, ymax=upper, group=country, fill = country),
                alpha=alp) +
    geom_line(data=irf, aes(x=V1, y=value, group=country), col = "black") +
    geom_hline(yintercept = 0, color = 'red') +
    facet_wrap(~variable, scales = scales, labeller = label_parsed) +
    xlab("Horizon") + ylab("Response") +
    scale_fill_viridis_d()+
    theme_bw()
}
