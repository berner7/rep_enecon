# Plot the time series of the extracted factors 

plot_factors <-  function(data){
  plot <-  set_names(data, names(data))%>%
    map(.,  as.data.frame)%>%
    bind_rows(., .id = "country")%>%
    gather(key = "timeseries", value = "value", -country, -time) %>%
    filter(timeseries %in% c("F1", "F2", "F3", "F4"))%>%
    #    mutate(value = exp(value/100))%>%
    ggplot(aes(x = time, y = value, col = timeseries))+
    geom_line()+
    theme(legend.position = "none")+
    facet_grid(rows = vars(factor(timeseries, levels = unique(timeseries))), 
               cols = vars(factor(country, levels = unique(country))), scales = "free")+
    theme_bw()+
    scale_y_continuous("Factors")+
    scale_x_continuous("Time (in Mont)")+
    theme(legend.position = "none") 
  return(plot)
}
