  datafac <- function(data, nfac, beobachtbare=us_data, inf=FALSE,  
                          fakmethod="2step", time = NULL){
    
    # PCA to get Factors
    pcfac <- function(data, numfac){
      
      ## PC factor extraction
      ## data must be (TxN)  
      nvars <- dim(data)[2]
      
      xx <- t(data)%*%data# X'X is proportional to covariance matrix
      eigen_x <- eigen(xx) 
      
      # applying the factor restricition
      lam <- sqrt(nvars)*eigen_x[[2]][,c(1:numfac)] # lam'lam/n = I. loadings are, where n = number of time series in x
      factors <- data%*%lam/nvars     
      
      return(list(factors, lam))
    }
    
  
    # adjust dimension for differencing
    nob <- dim(data)[1] # number of observations
    xvars <- dim(data)[2] # number of time series used for factor estimation
    
    ##### BEGIN FACTOR EXTRACTION PART #####
    twostepfac.fun <- function(data = data, beob = beobachtbare, numfac, standard = FALSE) {
      ## Input
      # data: Datamatrix
      # beob:  observed timeseries
      # numfac: number of factors
      # standard == TRUE, scale factors, yindex? 
      
      # data has to be of the form: TxN (T = # of observations, N = # of variables)
      ifelse(is.vector(beob)==TRUE, r <- 1, r <- dim(beob)[2])
      # Single value decomposition
      # Purge influence of observables from data
      olssvd <- function(y,ly){
        b          <- solve(t(ly)%*%ly)%*%t(ly)%*%y
        u <- y - ly%*%t(b)
        return(u)
      }
      
      data <- as.matrix(data)
      beob <- as.matrix(beob)
    
      x_means <- apply(data, 2, mean)
      x_sds <- apply(data, 2, sd)
      mean_mat <- matrix(rep(x_means, nob), ncol = xvars, byrow = TRUE)
      std_mat <- matrix(rep(x_sds, nob), ncol = xvars, byrow = T)
      x_sd <- (data - mean_mat)/std_mat
      
      # Factor Estimation
      temp <- pcfac(data=x_sd, nfac)
      factors <- temp[[1]]
      load <- temp[[2]]
      
      # Purge influence of GDP, Energy and Prices
      temp1 <- apply(factors, 2, function(x) olssvd(as.matrix(x), as.matrix(beob[,1])))
      temp2 <- apply(temp1, 2, function(x) olssvd(as.matrix(x), as.matrix(beob[,2])))
      temp3 <- apply(temp2, 2, function(x) olssvd(as.matrix(x), as.matrix(beob[,3])))
      
      # Select n largest Eigenvecs
      fac_est <- cbind(beob, temp3)
      load_est <- cbind(matrix(0,ncol = 3, nrow = nrow(load)), load)                        # dim F (TxK), loadings = (NxK)
      
      # Find out if that is correct... and why:
      return(list(factors = fac_est, loadings = load_est))
      }
   
    # colnames
    names <- c("time", "E", "Y", "P")
    for(i in 1:nfac){
      names <- append(names, paste0("F", i))
    }
    # FACTOR ESTIMATION
    facest <- twostepfac.fun(data = data, beob = beobachtbare, numfac = nfac) 
    return(list(fac_est = set_names(cbind(time, facest$factors), names),
                loadings = data.frame(variables = colnames(data),
                                      facest$loadings)))
    
  } 
  