# Factor Loadings plot
loadings_plot<- function(data, numfac = 2){
  # Reshape data frames
   temp <- set_names(data, names(data))%>%
    map(.,  as.data.frame)%>%
    bind_rows(., .id = "country")
  # rename factors
    ifelse(numfac == 2, colnames(temp) <- c("country", "variables", "X1", "X2", "X3", "F1", "F2"),
           colnames(temp) <- c("country", "variables", "X1", "X2", "X3", "F1", "F2", "F3"))
    # select most important loadings
    plot_list <- temp %>%     
    dplyr:: select(-X1, -X2, -X3)%>%
    gather(key = "timeseries", value = "value", -country, -variables) %>%
    mutate(var = as.numeric(as.factor(variables)))%>%
    group_by(country, timeseries)%>%
    top_n(n = 15, wt = abs(value)) %>%
    group_by(country)%>%
    group_split()
  
  p <- list()
  for(i in 1:length(plot_list)){
    p[[i]] <- ggplot(data = plot_list[[i]], aes(x= reorder(variables, -var), y = value, fill = factor(timeseries))) +
      scale_fill_discrete("Factor")+
      geom_bar(stat = "identity", position = "dodge2")+
      scale_y_continuous("Highest Factor Loadings of each factor")+
      scale_x_discrete(" ")+
      theme_bw()+
      coord_flip()+
      theme(axis.text.x = element_text(angle = 60, hjust = 1), legend.position = "none", legend.justification = "right")
  }
  p <- set_names(p, data$country)

  return(p)
}


