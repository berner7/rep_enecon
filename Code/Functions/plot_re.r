# Transform bootstrap data to rebound effect (including confidence bans) and plot effect
plot_re <- function(data, xlim = 24, mean_est = FALSE){
  boot_object <- data$boot_dc%>%
    set_names(., nm = data$country)
  
  # Define horizon
  horizon = unlist(map(boot_object[[1]][[1]], nrow))
  
  # Extract bootstrap and create confidence intervals
  CI_data <- lapply(boot_object, confidence_re)
  
  # Create data frame
  plot_data <- cbind(bind_rows(CI_data), country = rep(names(boot_object), each = horizon), 
                     Months = rep(0:(horizon-1), length(boot_object)))
  
  # Plot including confidence bans
  CI_plot <- ggplot(plot_data, aes(x = Months, ymin= lower, ymax= upper, y = impulse, group = country)) +  
    geom_ribbon( alpha=.3, fill = 'grey') +
    geom_line(aes(col = country), size= 1) + 
    geom_hline(yintercept = 0, color = 'red') +
    ylab("Response") +
    scale_color_viridis_d("")+
    scale_x_continuous(limits = c(0,xlim))+
    theme_bw()
  
  if(mean_est == TRUE){
    mean_est <- plot_data%>%
      group_by(Months)%>%
      dplyr::summarize(impulse = mean(impulse),
                lower = mean(lower),
                upper = mean(upper))
    
    CI_plot <- ggplot(plot_data, aes(x = Months)) +  
      geom_ribbon(alpha=.3, fill = 'lightgrey', aes(ymin= lower, ymax= upper, group = country)) +
      geom_ribbon(alpha=.3, fill = 'red', data = mean_est,
                  aes(ymin= lower, ymax= upper)) +
      geom_line(aes(col = country,y = impulse, group = country), size = 0.8) + 
      geom_line(data = mean_est, aes(y = impulse, x = Months), size =1.2, col = "darkred")+
      scale_size_manual(values = c(1,1,1,14,1,1))+
      geom_hline(yintercept = 0, color = 'black') +
      ylab("Response") +
      scale_color_viridis_d("")+
      scale_x_continuous(limits = c(0,xlim))+
      theme_bw()
    }
  
  return(CI_plot)
}
