plot_pca <- function(data){
  
  data_fac <- set_names(data, names(data))%>%
    map(.,  as.data.frame)%>%
    bind_rows(., .id = "country")%>%
    dplyr::select(-time, -E, -Y, -P)
  
  colnames(data_fac) <- c("country",1:10)
  
  plot <- data_fac %>%
    group_by(country)%>%
    dplyr::summarise_all(., function(x) sum(x^2))%>%
    gather(key = id, value = "value", -country) %>%
    mutate(id = as.numeric(id))%>%
    ggplot(aes(x = id, fill = country, y = value)) +
    geom_col(position = "dodge") +
    scale_fill_viridis_d("Country:") +
    scale_y_continuous("Variance explained [%]") +
    scale_x_continuous(breaks = 1:10) +
    xlab("Principal Component") +
    theme_bw() +
    theme(legend.justification = c(1, 1),
          legend.position = c(0.99, 0.99))
  
  return(plot)
}


