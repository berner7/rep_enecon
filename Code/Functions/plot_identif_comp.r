plot_identif_comp <- function(boots){
  horizon <- nrow(boots[[2]]$Germany$true$irf)
  boot_conf <- list()
  # Set names to boot objects
  for(i in 2:ncol(boots)){
    boots[[i]] <- set_names(boots[[i]], boots$country)
    boot_conf[[i-1]] <- map(boots[[i]], confidence_re)%>%
      bind_rows(.,.id = "country")%>%
      mutate(months = 1:horizon)
  }
  
  boot_conf <- set_names(boot_conf, names(boots)[2:4])
  plot_comp <- boot_conf %>%
    bind_rows(., .id = "identification")%>%
    ggplot(aes(x = months, y = impulse, group = identification)) +
    geom_line(aes(col = identification)) + 
    geom_hline(yintercept = 0, color = 'red') +
    scale_size_continuous(range = c(1,1.5), guide = FALSE)+
    scale_x_continuous(name = "Time [Months]")+
    xlab("Observation Time") + ylab("Rebound Effect") +
    facet_wrap(~country, scales = "free")+
    theme_bw()+
    scale_color_discrete("Model:")+
    theme(legend.position = "top", legend.justification = "right")
  
  return(plot_comp)
}
