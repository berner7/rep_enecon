## Prepare reduced form residuals for FAVAR model
# Data structure: 
#   - time
#   - E, Y, P 
#   - Factors

var_prep <- function(data, lag_max = 6, log = TRUE){
  # Normalize by variance
  data[,-1] <- apply(data[,-1], 2, function(x) x/var(x)) 
  # logarithmize
  if(log == TRUE){
    data[,2:4] <- apply(data[,2:4], 2, function(x) log(x)*100)  
  } 
  vars_eu <-  vars::VAR(data[,-1], lag.max = lag_max, ic = "AIC")
 
  p_value <- vars_eu$p
  # Output
  res_var <- residuals(vars_eu)
  
  return(list(vars = vars_eu, p = p_value, res = res_var))
}

