plot_sel_hd <- function(x, series = 1, p = 1, begin, end, date_of_interst_start){
  
  x <- svars::hd(x, series = series)
  Index <- seq.Date(as.Date(begin), as.Date(end), by = "month")
  value <- NULL
  PlotData <- as.data.frame(x$hidec)%>%
    mutate(time = seq.Date(as.Date(begin), as.Date("2019-09-01"), by = "month"))%>%
    dplyr::select(-V1)%>%
    filter(time == end | time == date_of_interst_start)%>%
    gather(key = "variable", value = "value", -time)%>%
    group_by(variable)%>%
    dplyr::summarise(value = 100*(value[2]-value[1]))%>%
    filter(!variable %in% c("Constructed series  E", "Demeaned series  E"))%>%
    mutate(variable2 = c("E", "F1", "F2", "P", "Y"))
  
  ggplot(PlotData, aes(x = variable2)) + 
    geom_bar(aes(y = value, fill = variable2), stat = "identity") +
    xlab("Time") + 
    theme_bw()+
    scale_fill_viridis_d("")+
    theme(legend.position = "none")
}


plot_sel_hd_all <- function(x, p = 1, series = 1, begin, end){
  x <- svars::hd(x, series = series)
  
  Index <- seq.Date(as.Date(begin), as.Date(end), by = "month")
  value <- NULL
  PlotData <- as.data.frame(x$hidec)%>%
    mutate(time = seq.Date(as.Date(begin), as.Date("2019-09-01"), by = "month"))%>%
    filter(time <= as.Date(end))%>%
    dplyr::select(-V1)%>%
    gather(key = "variable", value = "value", -time)
  
  p1 <- PlotData%>%
    filter(!variable %in% c("Demeaned series  E", "Constructed series  E"))%>%
    ggplot(., aes(y = value,x = time)) + 
    geom_bar(aes( fill = variable), stat = "identity") +
    xlab("Time") + 
    theme_bw()+
    scale_fill_viridis_d("")#+
 
  # p2 <- PlotData%>%
  #   filter(variable  == "Constructed series  E")%>%
  #   ggplot(., aes(x = time, y = value))+
  #   geom_line(lwd = 1.5, col = "red")+
  #   theme_bw()
  # 
  # cowplot::plot_grid(p1,p2, ncol = 2, rel_widths = c(1, 0.5), rel_heights = c(1, 0.5))
  # 
  p1
}
