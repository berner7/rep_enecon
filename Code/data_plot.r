#---------------------------------------------
# This code produces the data graph (Figure 2)
#---------------------------------------------
library(tidyverse)
library(reshape2)
library(cowplot)
library(ggplot2)
library(dplyr)

#-----------------------------------
# Paths
#-----------------------------------	
loadpath <- "Data/" #change this
savepath <- "Results/" #change this

#---------------------------------------
# Data
#---------------------------------------
# monthly data
data_mod1 <- readRDS(paste(loadpath, "energy_data_all.rds", sep=""))

# Plot all Datasets together
plot_e_y <- data_mod1 %>%
  mutate("Energy Intensity" = E/Y)%>%
  melt(., id.vars = c("time", "country"))%>%
  dplyr::filter(!variable %in% c("P", "E", "Y"))%>%
  mutate(value = (as.numeric(value)))%>%
  ggplot(.,aes(x = time, y = value, col = country, group = country))+ 
  geom_line(size = 2)+
  facet_wrap(.~variable, scales = "free", ncol = 2)+
  theme_bw()+
  ylab("Nat. Currency in Million/ktoe")+
  scale_color_viridis_d("")+
  scale_x_date()+
  xlab("")+
  theme_bw()+
  theme(legend.position = "top", legend.justification = "right")

data_plot_pr <- data_mod1 %>%
  melt(., id.vars = c("time", "country")) %>%
  dplyr::filter(variable == "P")%>%
  mutate(value = as.numeric(value),
         variable = recode(variable, "P" = "Prices"))%>%
  ggplot(.,aes(x = time, y = (value), col = country, group = country))+ 
  geom_line(size = 2)+
  facet_wrap(.~variable, scales = "free", ncol = 1)+
  theme_bw()+
  scale_y_continuous("National Currency")+
  scale_color_viridis_d("")+
  xlab("")+
  theme(legend.position = "none")

pdf(paste(savepath, "fig_2_data_timeseries.pdf", sep=""), width = 8, height = 5)
plot_grid(plot_e_y, data_plot_pr, ncol = 1, rel_heights = c(1.2, 1))
dev.off()

# Energy carriers/ mix
temp <- readRDS(paste0(loadpath, "energy_carriers_shares.rds"))
plot_stacked <- temp %>%
  mutate(time = as.yearmon(time))%>%
  filter(geo %in% countries & time <= as.yearmon("2019-12-01"))%>%
  mutate(geo = recode(geo, "Germany (until 1990 former territory of the FRG)" = "Germany"))%>%
  ggplot(aes(x = time, y = values, fill = product))+
  geom_area() +
  scale_fill_viridis_d("")+
  theme_bw()+
  theme(legend.position = "top", legend.justification = "right")+
  facet_wrap(.~geo, ncol = 2)
ggsave(paste0(savepath, "fig_g24_energy_mix.pdf"), plot_stacked,  width = 12, height = 10)




