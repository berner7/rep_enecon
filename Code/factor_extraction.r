#---------------------------------------------
# This code produces the results of the factor 
# extractions and plots the results
#---------------------------------------------

#-----------------------------------
# Packages
#-----------------------------------
library(dplyr); 
library(tidyr);  
library(zoo); 
library(ggplot2); 
library(plyr); 
library(purrr)
library(cowplot)

#-----------------------------------
# Paths
#-----------------------------------	
loadpath <- "Data/" #change this
savepath <- "Results/" #change this

#---------------------------------------
# Data
#---------------------------------------
# monthly data
data_mod1 <- readRDS(paste(loadpath, "energy_data_all.rds", sep=""))
# Data Matrix X
panel <- readRDS(paste(loadpath,"panel_stat.rds", sep = ""))

#---------------------------------------
# Functions
#---------------------------------------
# (1) Factor extraction 
source("Code/Functions/datafac.r")
# (2) Plot the principal components
source("Code/Functions/plot_pca.r")
# (3) Plot the estimated latent factors 
source("Code/Functions/plot_factors.r")
# (4) Plot the estimated loadings of factors 
source("Code/Functions/loadings_plot.r")

#---------------------------------------
# Factor estimation and plots
#---------------------------------------
# Extract factors (here 10)
favar_dat <- data_mod1%>%
  group_by(country)%>%
  nest()%>%
  mutate(panel = panel)%>%
  mutate(loadings = map2(panel, data, ~ datafac(data = .x, beobachtbare = .y[,-1],
                                                time = .y[,1], nfac = 10)),
         fac_est = map(loadings, "fac_est"))

# Plot principal components (Table 1)
pdf(paste(savepath, "tab_1_pca_explained_var.pdf", sep=""), width = 10, height = 7)
plot_pca(favar_dat$fac_est)
dev.off()

# Estimate factors and loadings for the FAVAR model (here 2)
numfac <- 2
favar_dat2 <- favar_dat %>%
  mutate(loadings = map2(panel, data, 
                         ~ datafac(data = .x, 
                                   beobachtbare = .y[,-1], 
                                   nfac = numfac, 
                                   time = .y[,1])),
         fac_est = map(loadings, "fac_est"),
         loadings_est = map(loadings, "loadings"))

# Plot factors (Figure 3)
factor_plots <-  plot_factors(favar_dat2$fac_est)
pdf(paste(savepath, "fig_3_est_latent_factors.pdf",sep = ""), width = 8, height = 4)
  factor_plots
dev.off()

# Plot the 15 highest factor loadings for the first two factors (Figure 4)
loadings_top15 <-  loadings_plot(favar_dat2$loadings_est, numfac = 2)
legend <- get_legend(loadings_top15[[1]])
# Loadings of Germany and the UK
p_uk <- loadings_top15[[4]]+ 
  theme(legend.position = "top", legend.justification = "right")
p_all <- plot_grid(plotlist = loadings_top15,  label_y = 1.04, ncol = 2, labels = names(loadings_top15))
p_ger <- plot_grid(legend, loadings_top15[[1]], ncol = 1, rel_heights = c(0.05, 0.95))
# Save plots
pdf(paste(savepath, "fig_4_loadings_uk_ger.pdf",sep = ""), width = 8, height =8)
plot_grid(p_ger, p_uk+xlab(" "), rel_heights = c(0.6, 0.8), labels = c("a","b"))
dev.off()
ggsave(paste(savepath, "fig_d16_loadings_all.pdf",sep = ""), p_all, width = 25, height = 15)

# Save factors in data
saveRDS(favar_dat2, "Data/favar_data_2factors.rds")
